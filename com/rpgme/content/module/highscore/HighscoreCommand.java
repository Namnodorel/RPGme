package com.rpgme.content.module.highscore;

import com.rpgme.content.module.highscore.HighscoreModule.Highscore;
import com.rpgme.content.module.highscore.HighscoreModule.HighscoreEntry;
import com.rpgme.content.skill.ExpTables;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.command.CommandHelp;
import com.rpgme.plugin.command.CoreCommand;
import com.rpgme.plugin.manager.SkillManager;
import com.rpgme.plugin.util.StringUtils;
import com.rpgme.plugin.util.Symbol;
import org.bukkit.command.CommandSender;

import java.text.MessageFormat;
import java.util.List;

public class HighscoreCommand extends CoreCommand {

	private static final String ENTRY_FORMAT = "&2{3} " + "{0} &f"+ Symbol.ARROW_RIGHT2 + " &alevel {1}" +  " &7({2} exp)",
			USAGE = "/top <skill>";

	private final HighscoreModule module;

	public HighscoreCommand(RPGme plugin, HighscoreModule module) {
		super(plugin, "rpgtop", "rpgme.command.top");
		this.module = module;
		setConsoleAllowed(true);
		setAliases("skilltop", "top", "highscore");
		setDescription("See the top players on the server for a skill");
        setCommandHelp(new CommandHelp("/top <skill>", "Show highscores for a given skill", "rpgme.command.top"));
	}

	@Override
	public void execute(CommandSender sender, String alias, List<String> flags) {

		SkillManager skillManager = plugin.getSkillManager();
		Skill skill = null;

		for(String s : flags) {
			if((skill = skillManager.getByName(s)) != null)
				break;
		}

		if(skill == null) {
			plugin.getCommandManager().printCommandHelp(this, sender);
			return;
		}   

		Highscore score = module.getHighscore(skill);

		score.updateExp();

		sender.sendMessage(buildHighscoreDisplay(score));
	}

	public String buildHighscoreDisplay(Highscore score) {
		int length = plugin.getConfig().getInt("Highscore length", 10);
		StringBuilder sb = new StringBuilder();


		// build header
		String title = "&2" + score.getSkill().getDisplayName() + " &fTop "+length;
		sb.append(buildTitle(title, 35)).append("\n");

		// build entries
		int i = 0;
		for(HighscoreEntry entry : score.getEntries()) {

			String name = plugin.getSkillManager().colorLight() + entry.getName();
			int level = ExpTables.getLevelAt(entry.getExp());
			char symbol = i < 10 ? Symbol.numbers[i++] : Symbol.FRAME_LINE.charAt(1);

			String line = MessageFormat.format(ENTRY_FORMAT, name, level, entry.getExp(), symbol);
			sb.append(line).append("\n");

		}
		
		if(i == 0) {
			sb.append("&8No data\n");
		}

		// build footnote
		sb.append("&7&m").append(org.apache.commons.lang3.StringUtils.repeat('-', 30));
		return StringUtils.colorize(sb.toString());
	}

	public static String buildTitle(String msg, int length) {
		int dashes = (int) Math.round((length - msg.length() - 2) / 2.0);

		return "&7&m" + org.apache.commons.lang3.StringUtils.repeat('-', dashes) + "&r " + msg + " &7&m" +  org.apache.commons.lang3.StringUtils.repeat('-', dashes);
	}

	//@Override
	public void addCommandHelp(CommandSender sender, List<String> list) {
		if(hasPermission(sender)) {
			list.add(USAGE + ":See the highscores for a skill");
		}
		
	}

	@Override
	public List<String> getTabComplete(CommandSender sender, String label,
			List<String> args) {
		return null;
	}

}
