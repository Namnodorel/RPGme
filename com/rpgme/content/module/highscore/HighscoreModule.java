package com.rpgme.content.module.highscore;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.ListenerModule;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.event.RPGLevelupEvent;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.config.YamlFile;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.File;
import java.util.*;

public class HighscoreModule implements ListenerModule {

	private final Set<Highscore> highscores = Sets.newHashSet();

    private final RPGme plugin;

    public HighscoreModule(RPGme plugin) {
        this.plugin = plugin;
    }

    @Override
    public String getName() {
        return "Highscores";
    }

    @Override
	public void onEnable() {
		YamlFile file = getFile();
		file.read();

		for(Skill skill : plugin.getSkillManager().getEnabledSkills()) {

			Highscore score = file.data.isList(skill.getName()) ? new Highscore(skill, file.data.getStringList(skill.getName())) : new Highscore(skill);
			highscores.add(score);			

		}

		new HighscoreCommand(plugin, this).register();
	}

	@Override
	public void onDisable() {
		YamlFile file = getFile();
		file.clear();

		for(Highscore score : highscores) {

			file.data.set(score.skill.getName(), score.toList());

		}
		file.write();
	}
	
	private YamlFile getFile() {
		return new YamlFile(new File(plugin.getUserDataFolder(), "highscores.dat"));
	}
	
	@Deprecated
	private YamlFile getLegacyFile() {
		return new YamlFile(plugin, "highscores.dat");
	}

	public Highscore getHighscore(Skill skill) {
		Highscore score = null;

		for(Highscore s : highscores) {
			if(s.getSkill() == skill) {
				score = s;
				break;
			}
		}
		if(score == null)
			throw new IllegalArgumentException("Skill "+skill.getName()+" is not enabled.");
		return score;
	}

	@EventHandler
	public void onLevelup(RPGLevelupEvent e) {
		Skill skill = e.getSkill();
		Highscore score = getHighscore(skill);

		if(score != null) {
			Player p = e.getPlayer();
			score.add(p.getName(), p.getUniqueId(), (int) plugin.getExp(p, skill.getId()));
		}
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		Player p = e.getPlayer();
        RPGPlayer rp = plugin.getPlayer(p);
        if(rp == null) return;

		for(Highscore score : highscores) {

			int exp = (int) rp.getExp(score.getSkill());
			score.add(p.getName(), p.getUniqueId(), exp);
			
		}
	}

	public static class Highscore {

		private final Skill skill;
		private final TreeSet<HighscoreEntry> entries = new TreeSet<HighscoreEntry>();

		public Highscore(Skill skill) {
			this.skill = skill;
		}
		
		public Highscore(Skill skill, List<String> data) {
			this(skill);
			for(String s : data) {
				String[] dat = s.split(" ");
				try {
					String name = dat[0];
					int exp = Integer.valueOf(dat[1]);
					UUID id = UUID.fromString(dat[2]);
					entries.add(new HighscoreEntry(name, id, exp));
				} catch(IllegalArgumentException ignore) { }
			}
		}

		public NavigableSet<HighscoreEntry> getEntries() {
			return entries.descendingSet();
		}

		public Skill getSkill() {
			return skill;
		}

		public void add(String name, UUID id, int exp) {
			// check if it already contains
			Iterator<HighscoreEntry> it = entries.iterator();
			while(it.hasNext()) {
				HighscoreEntry e = it.next();
				
				if(e.getId().equals(id)) {
					
					if(e.getName().equalsIgnoreCase(name)) {
						e.setExp(exp);
					} else {
						it.remove();
						entries.add(new HighscoreEntry(name, id, exp));
					}
					return;
				}
			}
			
			final int length = 10;
			
			if(entries.size() < length) {
				entries.add(new HighscoreEntry(name, id, exp));
			} else {

				int lowest = entries.first().getExp();
				if(exp > lowest) {
					entries.add(new HighscoreEntry(name, id, exp));
					if(entries.size() > length) {
						entries.pollFirst();
					}
				}
			}
		}

		public List<String> toList() {
			List<String> list = Lists.newArrayListWithCapacity(entries.size());

			for(HighscoreEntry e : this.entries) {
				String s = e.getName() + " " + e.exp + " " + e.id;
				list.add(s);
			}
			return list;
		}

		public void updateExp() {
			
			for(Player p : Bukkit.getOnlinePlayers()) {
				if(skill.isEnabled(p))
					add(p.getName(), p.getUniqueId(), (int) RPGme.getInstance().getExp(p, skill.getId()));
			}
			
		}

	}

	public static class HighscoreEntry implements Comparable<HighscoreEntry> {
		
		private final String username;
		private final UUID id;
		private int exp;

		public HighscoreEntry(String name, UUID id, int exp) {
			super();
			this.username = name;
			this.id = id;
			this.exp = exp;
		}
		
		public String getName() {
			return username;
		}

		public UUID getId() {
			return id;
		}

		public int getExp() {
			return exp;
		}

		public void setExp(int exp) {
			this.exp = exp;
		}

		@Override
		public int compareTo(HighscoreEntry arg0) {
			return Integer.compare(exp, arg0.exp);
		}
		
		

		
	}

}
