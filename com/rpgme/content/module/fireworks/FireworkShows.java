package com.rpgme.content.module.fireworks;

import com.google.common.collect.Lists;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.ListenerModule;
import com.rpgme.plugin.event.RPGLevelupEvent;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.effect.FireworkShow;
import com.rpgme.plugin.util.menu.ConfigurationMenu;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Robin on 12/02/2017.
 */
public class FireworkShows implements ListenerModule {

    private final RPGme plugin;
    private final List<int[]> smallUnlocks = Lists.newArrayList();
    private final List<int[]> largeUnlocks = Lists.newArrayList();

    public FireworkShows(RPGme plugin) {
        this.plugin = plugin;
        ConfigurationMenu.registerSetting(7, FireworkToggleSetting.class);
    }

    @Override
    public String getConfigDescription() {
        return "Spawn firework shows when players reach certain level milestones";
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        config.addValue("Define the milestones in the format everyXaboveY. Seperated with ;",
                "small_milestones", "every10above0;every5above70;every1above95")
                .addValue("large_milestones", "every25above0;every5above90");
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        String smalls = config.getString("small_milestones");
        String larges = config.getString("large_milestones");
        Pattern pattern = Pattern.compile("every(\\d+)above(\\d+)");

        for(String rule : smalls.split(";")) {
            try {
                Matcher matcher = pattern.matcher(rule);
                matcher.matches();

                String everyString = matcher.group(1);
                int every = Integer.valueOf(everyString);
                String aboveString = matcher.group(2);
                int above = Integer.valueOf(aboveString);

                smallUnlocks.add(new int[] { every, above});
            } catch (Exception e) {
                plugin.getLogger().log(Level.SEVERE, "Failed to read firework small milestone configuration '" + rule + "'", e);
            }
        }

        for(String rule : larges.split(";")) {
            try {
                Matcher matcher = pattern.matcher(rule);
                matcher.matches();

                String everyString = matcher.group(1);
                int every = Integer.valueOf(everyString);
                String aboveString = matcher.group(2);
                int above = Integer.valueOf(aboveString);

                largeUnlocks.add(new int[] { every, above});
            } catch (Exception e) {
                plugin.getLogger().log(Level.SEVERE, "Failed to read firework large milestone configuration '" + rule + "'", e);
            }
        }
    }

    protected boolean isSmallMilestone(int level) {
        for(int[] rule : smallUnlocks) {

            if(level >= rule[1] && level % rule[0] == 0)
                return true;
        }
        return false;
    }

    protected boolean isLargeMilestone(int level) {
        for(int[] rule : largeUnlocks) {

            if(level >= rule[1] && level % rule[0] == 0)
                return true;
        }
        return false;
    }

    @EventHandler
    public void onLevelup(RPGLevelupEvent e) {
        Player player = e.getPlayer();
        String setting = e.getRpgPlayer().getSetting("fireworks");

        if (LargeShow.currentTasks.contains(player) || SmallShow.currentTasks.contains(player))
            return;

        if (setting == null || !setting.equals("OFF")) {
            if (this.isLargeMilestone(e.getToLevel())) {
                new LargeShow(e.getPlayer());
            } else if (this.isSmallMilestone(e.getToLevel())) {
                new SmallShow(e.getPlayer());
            }

        }
    }

    protected static Location locAround(Player p, double range) {
        double degree = CoreUtils.random.nextDouble() * 3.141592653589793D * 2.0D;
        double x = Math.cos(degree) * range;
        double z = Math.sin(degree) * range;
        return p.getLocation().add(x, 0.0D, z);
    }

    private static class LargeShow extends BukkitRunnable {
        static final List<Player> currentTasks = Lists.newArrayList();//Sets.newHashSet();
        static final int[] TO_SKIP = new int[]{4, 7, 8, 12, 14, 17};
        static final int END = 18;
        final Player p;
        int i = 0;

        public LargeShow(Player p) {
            this.p = p;
            if (!currentTasks.contains(p)) {
                currentTasks.add(p);
                this.runTaskTimer(RPGme.getInstance(), 10L, 7L);
            }
        }

        @Override
        public void run() {
            ++i;

            for (int s : TO_SKIP) {
                if (i == s) {
                    return;
                }
            }

            if (i == END || !p.isValid()) {
                cancel();
                currentTasks.remove(p);
            }

            FireworkShow.spawnFireworks(locAround(p, Math.max(2.0D, (double) (i / 5))), 1);
        }
    }

    private static class SmallShow extends BukkitRunnable {

        static final List<Player> currentTasks = Lists.newArrayList();
        static final int[] TO_SKIP = new int[]{3, 4, 6};
        static final int END = 7;
        final Player p;
        int i = 0;

        public SmallShow(Player p) {
            this.p = p;
            if (!currentTasks.contains(p)) {
                currentTasks.add(p);
                runTaskTimer(RPGme.getInstance(), 10L, 8L);
            }

        }

        public void run() {
            i++;

            for (int s : TO_SKIP) {
                if (i == s) {
                    return;
                }
            }

            if (i == END || !p.isValid()) {
                cancel();
                currentTasks.remove(p);
            }
            FireworkShow.spawnFireworks(locAround(p, 3.0D), 1);

        }
    }
}