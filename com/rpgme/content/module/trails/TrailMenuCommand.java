package com.rpgme.content.module.trails;

import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.command.CoreCommand;
import com.rpgme.plugin.player.RPGPlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by Robin on 08/02/2017.
 */
public class TrailMenuCommand extends CoreCommand {

    private final SkillTrails manager;

    public TrailMenuCommand(SkillTrails manager) {
        super(manager.plugin, "rpgtrails", "rpgme.command.trails");
        this.manager = manager;
        setConsoleAllowed(false);
        setAliases("trails", "skilltrails", "masterytrails");
        setDescription("Open a GUI to select an mastery trail to use.\nIf you are skilled enough for it that is..");
    }

    @Override
    public void execute(CommandSender sender, String alias, List<String> flags) {
        Player player = (Player)sender;
        if(!canUse(player)) {
            String message = plugin.getMessages().getMessage("err_notunlocked", "You need at least one skill at level " + manager.masteryLevel);
            player.sendMessage(message);
        } else {
            new TrailMenu(manager, player).openGUI();
        }

    }

    private boolean canUse(Player player) {
        RPGPlayer rp = plugin.getPlayer(player);
        if(rp == null) return false;

        for(int skill : SkillType.values()) {
            if(rp.getLevel(skill) >= manager.masteryLevel) {
                return true;
            }
        }
        return false;
    }

    public void addCommandHelp(CommandSender sender, List<String> list) {
        if(sender instanceof Player && this.canUse((Player)sender)) {
            list.add("/trails:Opens up your mastery trail GUI!");
        }

    }

    @Override
    public List<String> getTabComplete(CommandSender sender, String label, List<String> args) {
        return null;
    }
}
