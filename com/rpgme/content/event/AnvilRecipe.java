package com.rpgme.content.event;

import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Robin on 10/09/2016.
 */
public interface AnvilRecipe {

    //Pair<ItemStack, ItemStack> getIngredients(InventoryView view);

    ItemStack getResult(InventoryView view);

    boolean matches(ItemStack[] ingredients);

    void removeIngredients(InventoryView view);

}
