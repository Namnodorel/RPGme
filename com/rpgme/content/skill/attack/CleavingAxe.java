package com.rpgme.content.skill.attack;

import com.rpgme.content.nms.NMS;
import com.rpgme.plugin.integration.PlayerRelation;
import com.rpgme.plugin.integration.PluginIntegration;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.Combat;
import com.rpgme.plugin.util.StringUtils;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.cooldown.Cooldown;
import com.rpgme.plugin.util.cooldown.VarEnergyPerSecCooldown;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class CleavingAxe extends Ability<Attack> implements Attack.AttackListener {
	
	private int unlocked;
	private Cooldown cooldown;
	private Scaler energy;

	public CleavingAxe(Attack skill) {
		super(skill, "Cleaving Axe", Attack.ABILITY_AXE);

	}

	@Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= unlocked) {
			double maxCooldown = 90 / energy.scale(forlevel);
			list.add("Cleaving Axe max cooldown:"+ StringUtils.readableDecimal(maxCooldown) + "s &7(5 uses)");
		}
	}

	@Override
	public void createConfig(ConfigBuilder config, BundleBuilder messages) {
		super.createConfig(config, messages);

		config.addValue("unlocked", 40);

		ConfigHelper.injectNotification(messages, getClass(), "");
	}

	@Override
	public void onLoad(ConfigurationSection config, BundleSection messages) {
		super.onLoad(config, messages);
		unlocked = getConfig().getInt("unlocked");

		cooldown = new VarEnergyPerSecCooldown(plugin, 20, 100);
		energy = new Scaler(unlocked, 1, 100, 5);

		addNotification(unlocked, Notification.ICON_UNLOCK, "Cleaving Axe", ConfigHelper.getNotification(messages, getClass(), ""));
	}

	@Override
	public boolean onInteractEntity(Player player, LivingEntity entity, int level) {

		ItemStack item = player.getInventory().getItemInMainHand();
		if(item == null || !item.getType().name().endsWith("_AXE"))
			return false;
		
		ability:
		if(level >= unlocked) {
			
			if(cooldown.isOnCooldown(player)) {
				sendOnCooldownMessage(player, cooldown.getMillisRemaining(player), false);
				break ability;
			}
			
			double regenSpeed = energy.scale(level);
			cooldown.add(player, (long) (regenSpeed * 1000));

			NMS.packets.doArmSwing(player);
			
			int damage = Combat.getDamageAttribute(item.getType()) + 1;
			for(Entity other : entity.getNearbyEntities(0.8, 0.8, 0.8)) {
				
				if(!(other instanceof Damageable) || other.getType() == EntityType.ARMOR_STAND)
					continue;
				
				if(other.getType() == EntityType.PLAYER && 
						PluginIntegration.getInstance().getRelation(player, (Player)other) == PlayerRelation.TEAM)
					continue;
				
				((Damageable)other).damage(damage * 0.5, player);
				
			}

            entity.getWorld().spawnParticle(Particle.EXPLOSION_NORMAL, entity.getEyeLocation().add(0, 1, 0), 10, 0.2, 0.2, 0.2, 0.15);
			player.getWorld().playSound(entity.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 0.4f, 1f);
			
		}
		
		return true;
	}
	
}
