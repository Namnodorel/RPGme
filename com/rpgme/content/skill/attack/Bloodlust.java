package com.rpgme.content.skill.attack;

import com.google.common.collect.Maps;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.StringUtils;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

public class Bloodlust extends Ability<Attack> {

	private final int delay = 2;
	private int unlock, lifestealUnlock, maxStacks;

	private Scaler damage, lifesteal;
	private final Map<UUID, Entry<Long, Integer>> map = Maps.newHashMap();

	//private final Set<Player> hasBorder = Sets.newHashSet();

	public Bloodlust(Attack skill) {
		super(skill, "Bloodlust", Attack.ABILITY_BLOODLUST);
	}

	@Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= unlock) {
			list.add("Max Bloodlust stacks:" + maxStacks);
			list.add("Attack Damage per stack:" + StringUtils.readableDecimal(getDamagePerStack(forlevel)));
			if(forlevel >= lifestealUnlock)
				list.add("Lifesteal per stack:" + StringUtils.readableDecimal(getLifestealPerStack(forlevel)) + "%");
		}

	}

	@Override
	public void createConfig(ConfigBuilder config, BundleBuilder messages) {
		super.createConfig(config, messages);
		config.addValue("unlocked", 30);
		config.addValue("upgrade", 55);
		config.addValue("max stacks", 6);

		ConfigHelper.injectNotification(messages, getClass(), 1);
		ConfigHelper.injectNotification(messages, getClass(), 2);
	}

	@Override
	public void onLoad(ConfigurationSection config, BundleSection messages) {
		super.onLoad(config, messages);

		unlock =config.getInt("unlocked");
		lifestealUnlock = config.getInt("upgrade");
		maxStacks = config.getInt("max stacks");

		damage = new Scaler(unlock, 2, 100, 12);
		lifesteal = new Scaler(lifestealUnlock, 2, 100, 8);

		addNotification(unlock, Notification.upgradableIcon(1, 2), getName(), ConfigHelper.getNotification(messages, getClass(), 1, delay));
		addNotification(lifestealUnlock, Notification.upgradableIcon(2, 2), getName(), ConfigHelper.getNotification(messages, getClass(), 2));
	}

	private double getDamagePerStack(int forlevel) {
		return damage.scale(forlevel)/maxStacks;
	}

	private double getLifestealPerStack(int forlevel) {
		return lifesteal.scale(forlevel)/maxStacks;
	}

	@EventHandler (priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onAttack(EntityDamageByEntityEvent e) {
		if(e.getDamager().getType() != EntityType.PLAYER || e.getEntityType() == EntityType.ARMOR_STAND)
			return;

		Player p = (Player) e.getDamager();
		if(!isEnabled(p))
			return;		
		
		long time = System.currentTimeMillis();
		int level = getLevel(p);

		if(level < unlock)
			return;

		UUID id = p.getUniqueId();


		Entry<Long, Integer> entry = map.get(id);

		if(entry == null || time - entry.getKey() > 1250 ) {
			add(id, time, 1);
		}
		else {

			int inARow = entry.getValue() + 1;

			if(inARow > delay) {

				int stacks = Math.min(maxStacks, inARow - delay);

//				//border effect
//				int percent = (inARow - delay) * 10;
//				NMS.packets.setWorldborderEffect(p, percent);
//				hasBorder.register(p);

				// register damage
				e.setDamage(e.getDamage() + (getDamagePerStack(level)*stacks));

                Location particleLoc = e.getEntity().getLocation().add(0, 1, 0);
                p.getWorld().spawnParticle(Particle.FLAME, particleLoc, stacks, 0.2, 1.0, 0.2, 0.03);
				// lifesteal
				if(level >= lifestealUnlock) {
					double health = e.getDamage() * (getLifestealPerStack(level)*stacks / 100);
					p.setHealth( Math.min(p.getMaxHealth(), p.getHealth() + health));
                    p.getWorld().spawnParticle(Particle.REDSTONE, particleLoc, stacks * 2, 0.5, 0.5, 0.5, 0.0);
				}
				
				GameSound.play(Sound.ENTITY_ENDERDRAGON_FLAP, p, 0.5f, 0.9f);
			}

			add(id, time, inARow);


		}

	}

	private void add(UUID id, long timestamp, int times) {
		map.put(id, new AbstractMap.SimpleEntry<Long, Integer>(timestamp, times));
	}
	
	private void remove(Player p) {
		map.remove(p.getUniqueId());
		//hasBorder.remove(p);
	}


	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		remove(e.getPlayer());
	}

	@EventHandler (priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onLeave(PlayerKickEvent e) {
		remove(e.getPlayer());
	}
	
	
}
