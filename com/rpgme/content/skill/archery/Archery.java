package com.rpgme.content.skill.archery;

import com.rpgme.content.nms.NMS;
import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.skill.BaseSkill;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.Combat;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.Id;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Button;
import org.bukkit.util.Vector;

import java.util.List;

public class Archery extends BaseSkill {
			
	private int unlock_arrowrecovery, unlock_headshot;
	
	private Scaler arrowVelocity = new Scaler(1, 0.8, 100, 1.6);
	private Scaler headshotDamage;
	private Scaler arrowRecoveryChance;

	public static final int ABILITY_DARTS = Id.newId();
	public static final int ABILITY_VOLLEY = Id.newId();
	
	public Archery() {
		super("Archery", SkillType.ARCHERY);
	}

	@Override
	public void onEnable() {
		registerAbility(ABILITY_DARTS, new Darts(this));
		registerAbility(ABILITY_VOLLEY, new Volley(this));
	}

	@Override
	public Material getItemRepresentation() {
		return Material.BOW;
	}

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder bundle) {
        super.createConfig(config, bundle);

        config.addValue("headshot unlocked", 15)
                .addValue("arrow_recovery unlocked", 5);
        ConfigHelper.injectNotification(bundle, getClass(), "_headshots");
        ConfigHelper.injectNotification(bundle, getClass(), "_arrows");
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);

        unlock_headshot = config.getInt("headshot unlocked");
        unlock_arrowrecovery = config.getInt("arrow_recovery unlocked");

        headshotDamage = new Scaler(unlock_headshot, 1.5, 100, 3);
        arrowRecoveryChance = new Scaler(unlock_arrowrecovery, 5, 100, 80);

        addNotification(unlock_headshot, Notification.ICON_PASSIVE, "Headshots", ConfigHelper.getNotification(messages, getClass(), "_headshots"));
        addNotification(unlock_headshot, Notification.ICON_PASSIVE, "Arrow Recovery", ConfigHelper.getNotification(messages, getClass(), "_arrows"));
    }

    @Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		list.add("Arrow velocity:x"+arrowVelocity.readableScale(forlevel));
		if(forlevel >= unlock_arrowrecovery)
			list.add("Arrow Recovery chance:"+arrowRecoveryChance.readableScale(forlevel) + '%');
		if(forlevel >= unlock_headshot)
			list.add("Headshot damage:x"+headshotDamage.readableScale(forlevel));
		
	}
	
	/**
	 * Gain exp for shooting entities (based on range)
	 */

	@EventHandler (priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onHit(EntityDamageByEntityEvent e) {
		
		if(e.getDamager() instanceof Arrow && e.getEntity() instanceof LivingEntity) {
			
			Arrow arrow = (Arrow) e.getDamager();
						
			if(arrow.getShooter() instanceof Player) {
				
				Player shooter = (Player) arrow.getShooter();
				if(!isEnabled(shooter))
					return;
				int level = getLevel(shooter);
				LivingEntity target = (LivingEntity) e.getEntity();
								
				// headshot damage
				if(level >= unlock_headshot && isHeadshot(arrow.getLocation(), target)) {
					
					double multiplier = headshotDamage.scale(level);
					e.setDamage(e.getDamage() * multiplier);
					GameSound.play(Sound.ENTITY_ARROW_HIT_PLAYER, shooter, 1f, 0.6f);
					GameSound.play(Sound.ENTITY_ARROW_HIT_PLAYER, target.getLocation(), 1f, 0.6f);
					
				}
				
				// arrow recovery
				if(level >= unlock_arrowrecovery && NMS.isHooked() && arrowRecoveryChance.isRandomChance(level) && NMS.util.canPickup(arrow)) {
					arrow.getWorld().dropItem(arrow.getLocation(), new ItemStack(Material.ARROW));
					GameSound.play(Sound.ENTITY_CHICKEN_EGG, arrow.getLocation());
				}
				
				float exp = Combat.getArcheryDamageExp(target, e.getFinalDamage(), target.getLocation().distanceSquared(shooter.getLocation()));
				addExp(shooter, exp);
				
			}
			
		}
		
	}
	
	private boolean isHeadshot(Location spot, LivingEntity target) {	
		double arrowHeight = spot.getY() - target.getLocation().getY();
		double eyeHeight = target.getEyeHeight(false);
				
		return arrowHeight > eyeHeight - 0.15 && arrowHeight < eyeHeight + 0.3; 
				
	}
	
	/**
	 * Gain exp for killing entities
	 */
	
	@EventHandler
	public void onKillingBlow(EntityDeathEvent e) {
		Player killer = e.getEntity().getKiller();
		if(killer != null && e.getEntity().getLastDamageCause().getCause() == DamageCause.PROJECTILE && isEnabled(killer)) {
			
			addExp(killer, Combat.getKillExp(e.getEntity()));
		}
	}
	
	/*
	 * Scale arrow velocity
	 */
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onShoot(EntityShootBowEvent e) {
		if(e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			if(!isEnabled(p))
				return;
			
			double velocity = arrowVelocity.scale(getLevel(p));
			Vector vector = p.getLocation().getDirection().multiply(velocity * e.getProjectile().getVelocity().length());

			e.getProjectile().setVelocity(vector);
		}
	}
	
	/*
	 * Undo velocity change (otherwise damage would drastically increase)
	 */
	@EventHandler
	public void onShoot(ProjectileHitEvent e) {
		if(e.getEntity() instanceof Arrow && e.getEntity().getShooter() instanceof Player) {
		
			Player p = (Player) e.getEntity().getShooter();
			if(!isEnabled(p))
				return;
			
			double velocity = arrowVelocity.scale(getLevel(p));
			if(velocity > 1)
				e.getEntity().setVelocity(e.getEntity().getVelocity().multiply(1/velocity));
		}
	}
	
	/*
	 * Gain exp from shooting a button (based on range)
	 */
	
	@EventHandler
	public void onShootButton(ProjectileHitEvent e) {
		
		if(e.getEntityType() != EntityType.ARROW || !(e.getEntity().getShooter() instanceof Player))
			return;
		
		Location loc = e.getEntity().getLocation();
		
		if(loc.getBlock().getType() == Material.WOOD_BUTTON) {
			
			// logic to see if buton is hit
			double x = Math.abs( Math.abs(loc.getX()) - Math.abs(loc.getBlockX()) );
			double y = loc.getY() - loc.getBlockY();
			double z = Math.abs( Math.abs(loc.getZ()) - Math.abs(loc.getBlockZ()) );
			
			BlockFace face = ((Button) loc.getBlock().getState().getData()).getAttachedFace();
			boolean hasHit;
			
			// check y and z
			if(face == BlockFace.EAST || face == BlockFace.WEST) {
				
				hasHit = isOnButtonY(y) && isOnButtonXZ(z);
				
			// check y and x
			} else if(face == BlockFace.NORTH || face == BlockFace.SOUTH) {
				
				hasHit = isOnButtonY(y) && isOnButtonXZ(x);
			
			// check x and z
			} else { // face == BlockFace.UP || face == BlockFace.DOWN 
				
				hasHit = isOnButtonXZ(x) && isOnButtonXZ(z);
				
			}
			
			if(hasHit) {
				
				Player p = (Player) e.getEntity().getShooter();
				GameSound.play(Sound.ENTITY_ARROW_HIT_PLAYER, p.getLocation());
				
				if(isEnabled(p)) {
				double distance = loc.distance(p.getLocation());
				
				int xp = (int) Math.min((distance - 15), 15)+5;
				if(xp > 5)
					addExp(p, xp);
				}
			}
		}
	}
	
	private boolean isOnButtonY(double y) {
		return y > 0.3 && y < 0.7;
	}
	
	private boolean isOnButtonXZ(double xz) {
		return xz > 0.22 && xz < 0.78;
	}
	
}
