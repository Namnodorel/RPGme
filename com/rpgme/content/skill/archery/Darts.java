package com.rpgme.content.skill.archery;

import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.cooldown.VarMaxEnergyCooldown;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.List;


public class Darts extends Ability<Archery> {
		
	private int UNLOCK, POISON_UNLOCK;
	private Scaler maxEnergy, velocity;
	private VarMaxEnergyCooldown cooldown;

	public Darts(Archery skill) {
		super(skill, "Darts", Archery.ABILITY_DARTS);
	}

	@Override
	public void createConfig(ConfigBuilder config, BundleBuilder messages) {
		super.createConfig(config, messages);
		config.addValue("unlocked", 15);
		config.addValue("poisoned", 40);

		ConfigHelper.injectNotification(messages, getClass(), 1);
		ConfigHelper.injectNotification(messages, getClass(), 2);
	}

	@Override
	public void onLoad(ConfigurationSection config, BundleSection messages) {
		super.onLoad(config, messages);
		UNLOCK = config.getInt("unlocked");
		POISON_UNLOCK = config.getInt("poisoned");

		cooldown = new VarMaxEnergyCooldown(plugin, 1, 10);
		maxEnergy = new Scaler(UNLOCK, 30, 100, 60);
		velocity = new Scaler(UNLOCK, 1.5, 100, 2.2);

		addNotification(UNLOCK, Notification.upgradableIcon(1, 2), getName(), ConfigHelper.getNotification(messages, getClass(), 1));
		addNotification(UNLOCK, Notification.upgradableIcon(2, 2), "Poisoned " + getName(), ConfigHelper.getNotification(messages, getClass(), 2));
	}

	@Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= UNLOCK)
			list.add("Darts Energy:"+(int)Math.round(maxEnergy.scale(forlevel)) + "&7 costs 12");
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onThrow(PlayerInteractEvent e) {

		if(!e.hasItem() || e.getAction() == Action.PHYSICAL || e.getItem().getType() != Material.ARROW)
			return;
		
		Player p = e.getPlayer();
		if(!isEnabled(p))
			return;

		int level = skill.getLevel(p);
		
		if(level >= UNLOCK) {

            // do cooldown
			if(cooldown.isOnCooldown(p)) {
				sendOnCooldownMessage(p, cooldown.getMillisRemaining(p));
				return;
			}
            int maxCooldownEnergy = (int) Math.round(maxEnergy.scale(level));
            cooldown.add(p, maxCooldownEnergy);

			// take arrow and spawn
            PlayerInventory inv = p.getInventory();
            if(e.getHand() == EquipmentSlot.HAND) {
                ItemStack item = inv.getItemInMainHand();
                if(item.getType() == Material.ARROW) {
                    item.setAmount(item.getAmount() - 1);
                    inv.setItemInMainHand(item);
                    spawnArrow(p, level);
                }
            }
            else if(e.getHand() == EquipmentSlot.OFF_HAND) {
                ItemStack item = inv.getItemInOffHand();
                if(item.getType() == Material.ARROW) {
                    item.setAmount(item.getAmount() - 1);
                    inv.setItemInOffHand(item);
                    spawnArrow(p, level);
                }
            }
		}
		
	}

    public void spawnArrow(Player p, int level) {
        // spawn arrow
        Vector dir = CoreUtils.addNaturalOffset(p.getLocation().getDirection().multiply(velocity.scale(level)), 0.05);
        Arrow a = p.launchProjectile(Arrow.class, dir);
        a.setCritical(true);

        // do poison
        if(level >= POISON_UNLOCK && CoreUtils.random.nextBoolean()) {
            GameSound.play(Sound.BLOCK_SLIME_HIT, p.getLocation(), 1f, 1f);
            a.setMetadata("PoisonArrow", new FixedMetadataValue(plugin, true));
        }
    }
	
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onHit(EntityDamageByEntityEvent e) {
		if(e.getCause() == DamageCause.PROJECTILE && e.getDamager().hasMetadata("PoisonArrow")) {
			
			LivingEntity entity = (LivingEntity) e.getEntity();
			entity.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 60, 1, false, true), true);
		}
	}

}
