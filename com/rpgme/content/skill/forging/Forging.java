package com.rpgme.content.skill.forging;

import com.rpgme.content.event.AnvilCraftEvent;
import com.rpgme.content.skill.ExpTables;
import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.skill.BaseSkill;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.Id;
import com.rpgme.plugin.util.ItemCategory;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.ItemStack;

public class Forging extends BaseSkill {

    public static final int ABILITY_QUICK_FIX = Id.newId();
    public static final int ABILITY_DECONSTRUCT = Id.newId();
    public static final int ABILITY_FIELD_REPAIR = Id.newId();

	public Forging() {
		super("Forging", SkillType.FORGING);
	}

    @Override
    public void onEnable() {
        registerAbility(ABILITY_DECONSTRUCT, new Deconstruct(this));
        registerAbility(ABILITY_QUICK_FIX, new QuickFix(this));
        registerAbility(ABILITY_FIELD_REPAIR, new FieldRepair(this));
    }

    @Override
    public Material getItemRepresentation() {
        return Material.WORKBENCH;
    }

    /*
	 * gain forging exp for
	 * - crafting tools,weapons,armor
	 * - repairing, either in crafting matrix or on anvil
	 */

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onCraft(final CraftItemEvent e) {
		if((e.getAction() != InventoryAction.PICKUP_ALL && e.getAction() != InventoryAction.MOVE_TO_OTHER_INVENTORY) || e.getResult() != Result.ALLOW)
			return;

		final Player p = (Player) e.getWhoClicked();

		if(!isEnabled(p))
			return;

		// Fixed exploit
		if(e.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY && e.getWhoClicked().getInventory().firstEmpty() == -1) {
			return;
		}

		ItemStack result = e.getCurrentItem();
		String mat = result.getType().name().toLowerCase();
		float xp = 0;

		// check if items are combined to increase durability
		short durabilityRepaired = findRepairDifference(e.getClickedInventory().getContents(), result);
		if(durabilityRepaired >= 0) { 

			xp = getRepairExp(result.getType(), durabilityRepaired);
			
		} else {

			// check if created bow
			if(result.getType() == Material.BOW) {

				xp = 12.5f;

			} else {
				// else check the material and item
				int seperator = mat.indexOf('_');
				if(seperator == -1)
					return;

				int ingots = getIngotsUsed(mat.substring(seperator+1));
				if(ingots == 0)
					return;

				xp = (float) (ExpTables.getForgingExp(mat.substring(0, seperator)) * ingots);

			}
			
			int amount = 1;
			// check if is shift clicking and multiply xp
			if(e.isShiftClick()) {

				amount = 65;
				ItemStack[] contents = e.getInventory().getContents();

				// starting at one so we don't getModule the result
				for(int i = 1; i < contents.length; i++) {
					if(contents[i].getType() != Material.AIR)
						amount = Math.min(amount, contents[i].getAmount());
				}

				if(amount < 65) // sanity check
					xp *= amount;
			}
			// armor expertice
//			if(experticeModule != null)
//				experticeModule.onCraftItem(p, result.getType(), amount);
			
		}
		// register exp
		GameSound.play(Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, p, 1f, 1.35f, 0.42);
		addExp(p, xp);
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onAnvilRepair(AnvilCraftEvent e) {
		if(e.isRepair()) {

			Player p = e.getPlayer();
			if(!isEnabled(p))
				return;

			float exp;

//			// check for our custom recipes
//			AnvilRecipe customRecipe = e.getRecipe();
//			if(customRecipe != null) {
//
//				if(customRecipe instanceof ItemUpgrade) {
//					exp = (((ItemUpgrade)customRecipe).unlocked + 20) / 4f; // exp based on unlock level
//				} else {
//					exp = 0f;
//				}
//
//			} else {

				// else treat like a (vanilla) repair
				ItemStack result = e.getCraftResult();
				short repaired = (short) (e.getCraftOrigional().getDurability() - result.getDurability());
			
				exp = getRepairExp(result.getType(), repaired);
			//}
			addExp(p, exp);
			GameSound.play(Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, p, 1f, 1.35f, 0.42);

		}

	}
	
	private short findRepairDifference(ItemStack[] matrix, ItemStack result) {
		Material type = result.getType();
		short highest = 0;

		for(int i = 1; i < matrix.length; i++) {
			ItemStack item = matrix[i];
			if(item == null || item.getType() == Material.AIR)
				continue;
			
			if(item.getType() != type)
				return -1;
			
			highest = (short) Math.max(highest, item.getDurability());
			
		}
		
		return (short) (highest - result.getDurability());
	}
	
	public float getRepairExp(Material material, short durRepaired) {
		if(durRepaired < 1 || material.getMaxDurability() < 1)
			return 0f;
		
		double totalExp = ExpTables.getForgingExp(material.name().split("_")[0].toLowerCase());
		double repairPercent = durRepaired / (double) material.getMaxDurability();
		
		return (float) (totalExp * repairPercent * 5);
	}

//	private double getMaxRepairExp(Material material) {
//		String[] name = material.name().split("_");
//
//		return name.length > 1 ? ExpTables.getForgingExp(name[0].toLowerCase()) * 2 : 25;
//	}
//	
//	public float getRepairExp(int durDifference, int totalEnchants) {
//		return (totalEnchants * Math.min(durDifference / 50f, 3f) + (durDifference * 0.3f));
//	}

	public static boolean isWeapon(ItemStack item) {
		return ItemCategory.WEAPON.isItem(item);
	}

	public static Material getRepairMaterial(String prefix) {
		switch(prefix.toLowerCase()) {

		case "leather": return Material.LEATHER;
		case "wood": return Material.WOOD;
		case "stone": return Material.COBBLESTONE;
		case "iron":
		case "chainmail": return Material.IRON_INGOT;
		case "gold": return Material.GOLD_INGOT;
		case "diamond": return Material.DIAMOND;
		default: return null;
		}
	}

	public static int getIngotsUsed(String suffix) {
		switch(suffix.toLowerCase()) {

		case "spade": return 1;
		case "hoe":
		case "sword": return 2;
		case "axe":
		case "pickaxe": return 3;
		case "boots": return 4;
		case "helmet": return 5;
		case "leggings": return 7;
		case "chestplate": return 8;

		default: return 0;
		}
	}




}
