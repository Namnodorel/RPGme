package com.rpgme.content.skill.stamina;

import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.StringUtils;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.FoodLevelChangeEvent;

import java.util.List;


/**
 *
 */
public class FoodReduction extends Ability<Stamina> {

    protected Scaler foodReduction = new Scaler(10, 5, 100, 75);

    public FoodReduction(Stamina skill) {
        super(skill, "Food reduction", Stamina.ABILITY_FOOD_REDUCTION);
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        // always call the super config
        super.createConfig(config, messages);
        // define the unlock notification
        messages.addValue("notification", "A true survivalist can survive with minimal food.. And so can you! " +
                "Your hunger level will reduce slower than normal, up to " + foodReduction.maxvalue + "%!");
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        // always call the super config
        super.onLoad(config, messages);
        // load and register the notification
        String notificationText = messages.getMessage("notification");
        addNotification(Notification.builder().title(getName()).level(foodReduction.minlvl).icon(Notification.ICON_PASSIVE).text(notificationText).build());
    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {
        // Here we create the value to appear in the /stamina command.
        if(forlevel > foodReduction.minlvl)
            // the format to add to the list is name:value
            list.add("Food Consumption:-"+ StringUtils.readableDecimal(foodReduction.scale(forlevel))+'%');
    }


    /**
     * Now for the actual effect, a listener
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onFoodChange(FoodLevelChangeEvent e) {
        Player p = (Player)e.getEntity();
        // always check if the player is enabled before using an ability
        if(!isEnabled(p))
            return;

        int previous = p.getFoodLevel();
        int now = e.getFoodLevel();

        if(previous > now) {

            int level = getLevel(p);
            if(level > foodReduction.minlvl && foodReduction.isRandomChance(level)) {
                e.setCancelled(true);
            }

        }
    }

}
