package com.rpgme.plugin.blockmeta;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.blockmeta.data.BooleanStorageLayer;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.*;

/**
 * A listener that tracks if blocks are placed by players.
 * Only blocks of registered types are tracked.
 *
 * Use this class with the instance kept by the {@link BlockDataManager} module
 */
public class PlayerPlacedListener extends BlockDataListener implements Runnable {
	
	public static final String PLAYER_PLACED = "PlayerPlaced";

    public static PlayerPlacedListener getInstance() {
        BlockDataManager manager = RPGme.getInstance().getModule(RPGme.MODULE_BLOCK_META);
        return manager.getPlayerPlacedListener();
    }
	
	private final Set<Material> trackables = EnumSet.noneOf(Material.class);
    private final List<Block> toRemove = new ArrayList<Block>(4);
    private BlockDataManager dataStore;

	protected PlayerPlacedListener(RPGme plugin) {
		super(plugin, PLAYER_PLACED);
	}
	
	protected BlockDataManager getDataStore() {
	    if(dataStore == null) {
            dataStore = plugin.getModule(RPGme.MODULE_BLOCK_META);
            dataStore.addEmptyLayer(PLAYER_PLACED, String.class);
        }
        return dataStore;
    }

    public void registerMaterial(Material... materials) {
        Collections.addAll(trackables, materials);
    }
	
	@Override
	public void registerListeners() {
		super.registerListeners();
		plugin.getServer().getScheduler().runTaskTimer(plugin, this, 200l, 3l);
	}
	
	
	public boolean isPlayerPlaced(Block block) {
		return ((BooleanStorageLayer) getDataStore().getLayer(block.getWorld(), PLAYER_PLACED)).getData(block);
	}
	
	public boolean collectPlayerPlaced(Block block) {
		return getDataStore().collect(block, PLAYER_PLACED, Boolean.class);
	}
	
	public void setPlayerPlaced(Block block, boolean value) {
		getDataStore().set(block, PLAYER_PLACED, value);
	}

	@Override
	public void run() {
		if(!toRemove.isEmpty()) {
			for(Block block : toRemove) {
				getDataStore().remove(block, PLAYER_PLACED);
			}
			toRemove.clear();
		}
	}

	@Override
	public void onBreak(Block block, Player player) {
		if(block.isEmpty() || !trackables.contains(block.getType())) return;
		toRemove.add(block);
	}

	@Override
	protected void onPiston(List<Block> blocks, BlockFace direction) {
	    List<Integer> playerPlacedIndeces = new ArrayList<>(blocks.size());
        for(int i = 0; i < blocks.size(); i++) {
            Block block = blocks.get(i);
            if(trackables.contains(block.getType()) && getDataStore().collect(block, PLAYER_PLACED, Boolean.class)) {
                playerPlacedIndeces.add(i);
            }
        }
        playerPlacedIndeces.forEach((i) -> getDataStore().set(blocks.get(i).getRelative(direction), PLAYER_PLACED, true));
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlace(BlockPlaceEvent event) {
		if(trackables.contains(event.getBlock().getType())) {
			setPlayerPlaced(event.getBlock(), true);
		}
	}
	

}
