package com.rpgme.plugin.treasure;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.ItemUtils;
import com.rpgme.plugin.util.RPGRunnable;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Robin on 26/02/2017.
 */
public class TreasureRoll extends RPGRunnable {

    private final Player p;
    protected final Inventory inv;
    private final ItemStack[] loot;
    int i = 0;
    int step = 0;

    public TreasureRoll(Player p, Inventory inv, int forlevel) {
        this.p = p;
        this.inv = inv;
        this.loot = new ItemStack[12];
        TreasureBag bag = RPGme.getInstance().getModule(RPGme.MODULE_TREASURES);

        for(int i = 0; i < 12; ++i) {
            loot[i] = bag.rollTreasureItem(forlevel);
        }
    }

    @Override
    public void run() {
        ++i;
        if(i > 75) {
            if(i == 90) {
                finish();
                p.closeInventory();
            }

            GameSound.play(Sound.ENTITY_EXPERIENCE_ORB_PICKUP, p, 2.0F, 1.2F, 0.4D);
        } else {
            if(i > 60) {
                if(i % 7 != 0) {
                    return;
                }
            } else if(i > 50) {
                if(i % 5 != 0) {
                    return;
                }
            } else if(i > 30 && i % 3 != 0) {
                return;
            }

            rollDown();
            GameSound.play(Sound.BLOCK_WOOD_BREAK, p, 0.3D);
        }
    }

    private void rollDown() {
        ItemStack next = loot[++step % loot.length];
        inv.setItem(22, inv.getItem(13));
        inv.setItem(13, inv.getItem(4));
        inv.setItem(4, next);
    }

    @Override
    public void finish() {
        ItemStack result = i == 90 ? inv.getItem(13) : loot[11];
        ItemUtils.give(p, result);
        GameSound.play(Sound.BLOCK_NOTE_PLING, p);
        TreasureChest.currentRolls.remove(this);
    }
}