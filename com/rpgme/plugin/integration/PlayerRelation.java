package com.rpgme.plugin.integration;

public enum PlayerRelation {
	
	SELF,
	
	TEAM,
	NEUTRAL,
	ENEMIES,

}
