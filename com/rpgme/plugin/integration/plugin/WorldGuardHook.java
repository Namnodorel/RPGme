package com.rpgme.plugin.integration.plugin;

import com.rpgme.plugin.integration.PluginIntegration.BlockProtectionPlugin;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class WorldGuardHook implements BlockProtectionPlugin {
	
	private WorldGuardPlugin wg;
	
	public WorldGuardHook() {
		
	}
	
	@Override
	public void enable(Plugin plugin) {
		wg = (WorldGuardPlugin) plugin;
	}

	@Override
	public boolean canChange(Player p, Block block) {
		return wg.canBuild(p, block);
	}
	
	@Override
	public boolean isInClaim(Block block) {
		return wg.getRegionManager(block.getWorld()).getApplicableRegions(block.getLocation()).size() > 0;
	}

	@Override
	public String getPluginName() {
		return "WorldGuardHook";
	}

	public boolean allowPvP(Player player, Location location) {
        LocalPlayer localPlayer = wg.wrapPlayer(player);

        RegionManager regionManager = wg.getRegionManager(player.getWorld());
        if(regionManager == null)
            return true;

        ApplicableRegionSet set = regionManager.getApplicableRegions(location);
        StateFlag.State state = set.queryState(localPlayer, DefaultFlag.PVP);
        return state == null || state == StateFlag.State.ALLOW;
    }
	

}
