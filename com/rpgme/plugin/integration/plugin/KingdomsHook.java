package com.rpgme.plugin.integration.plugin;

import com.rpgme.plugin.integration.PlayerRelation;
import com.rpgme.plugin.integration.PluginIntegration.BlockProtectionPlugin;
import com.rpgme.plugin.integration.PluginIntegration.TeamPlugin;
import me.leothepro555.kingdoms.main.Kingdoms;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import static com.rpgme.plugin.integration.PlayerRelation.*;

public class KingdomsHook implements BlockProtectionPlugin, TeamPlugin {
	
	private Kingdoms plugin;
		
	@Override
	public void enable(Plugin plugin) {
		if(!(plugin instanceof Kingdoms)) return;
		this.plugin = (Kingdoms) plugin;
	}

	@Override
	public PlayerRelation getRelation(Player one, Player two) {
		String k1 = plugin.getKingdom(one), k2 = plugin.getKingdom(two);
		
		if(k1 == null || k2 == null) return NEUTRAL;
		if(k1.equals(k2)) return TEAM;
		
		if(plugin.isEnemy(k1, two) || plugin.isEnemy(k2, one)) return ENEMIES;
		return NEUTRAL;
	}

	@Override
	public boolean canChange(Player p, Block block) {
		String playerKingdom = plugin.getKingdom(p);
		String blockKingdom = plugin.getChunkKingdom(block.getChunk());
		
		return playerKingdom == blockKingdom;
	}

	@Override
	public boolean isInClaim(Block block) {
		return plugin.getChunkKingdom(block.getChunk()) != null;
	}

	@Override
	public String getPluginName() {
		return plugin.getName();
	}
	
	

}
