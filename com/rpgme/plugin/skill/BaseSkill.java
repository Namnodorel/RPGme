package com.rpgme.plugin.skill;

import com.google.common.collect.Sets;
import com.rpgme.content.skill.ExpTables;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.manager.SkillManager;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.IntMap;
import com.rpgme.plugin.util.PermissionChecker;
import com.rpgme.plugin.util.StringUtils;
import com.rpgme.plugin.util.Symbol;
import com.rpgme.plugin.util.config.*;
import com.rpgme.plugin.util.effect.HarpPlingRunnable;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

import java.util.*;
import java.util.logging.Level;


public abstract class BaseSkill implements Skill, StatProvider {

    // plugin
    protected final SkillManager manager;
    protected final RPGme plugin;
    // collections
    private final SortedSet<Notification> notifications = Sets.newTreeSet();
    private final IntMap<Ability> abilityMap = new IntMap<>();
    // constants
    private final String name;
    private final int id;

    // configurations
    protected ConfigurationSection config;
    protected MessagesBundle messages;

    // cached configurables
    protected double expModifier;
    protected int expLimit;
    protected String displayName;

    public BaseSkill(String name, int id) {
        this.name = name;
        this.id = id;
        plugin = RPGme.getInstance();
        manager = plugin.getSkillManager();
    }

    /** {@inheritDoc} */
    @Override
    public void onEnable() {

    }

    /** {@inheritDoc} */
    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {

    }

    /** {@inheritDoc} */
    @Override
    public void onDisable() {
        for(Ability ability : abilityMap.values()) {
            ability.onDisable();
        }
    }

    /** {@inheritDoc} */
    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder bundle) {
        //config.addValue("enabled", true);
        config.addValue("exp multiplier", 1.0);
        config.addValue("exp limit", -1);

        bundle.addValue("name", name);
        ConfigHelper.injectMessage(bundle, "description_" + getName().toLowerCase(), "description");
        bundle.addValue("url", "https://gitlab.com/Flamedek/RPGme/wikis/" + getName());

        for(Ability ability : abilityMap.values()) {
            bundle.setPrefix(getName() + "_" + ability.getName());
            config.beginSection(ability.getName().replace(" ", ""));
            try {
                ability.createConfig(config, bundle);
            } catch (Exception e) {
                getPlugin().getLogger().log(Level.SEVERE, "Skill " + name + " failed to initialize.", e);
            }
            config.endSection();
        }

        bundle.setPrefix(getName());
    }

    /** {@inheritDoc} */
    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        this.config = config;
        this.messages = messages;

        expModifier = config.getDouble("exp multiplier");
        expLimit = (int) config.getDouble("exp limit");
        displayName = messages.getMessage("name");
        if(displayName.equals(MessagesBundle.MESSAGE_UNKNOWN)) {
            displayName = "";
        }

        for(Ability ability : abilityMap.values()) {
            ConfigurationSection configSection = config.getConfigurationSection(ability.getName().replace(" ", ""));
            BundleSection bundleSection = messages.getSection(ability.getName());
            try {
                ability.onLoad(configSection, bundleSection);
            } catch (Exception e) {
                getPlugin().getLogger().log(Level.SEVERE, "Skill " + name + " failed to initialize.", e);
            }
        }

        for(Ability ability : abilityMap.values()) {
            if(!ability.isEnabled()) {
                removeAbility(ability.getId());
            }
        }
    }

    @Override
    public String getConfigDescription() {
        int availableLength = 25 * 2 + 2 - 6;
        int titleLength = getName().length();
        int preLength = (availableLength - titleLength) / 2;
        int postLength = availableLength - titleLength - preLength;

        return org.apache.commons.lang3.StringUtils.repeat('=', preLength) +
                "  " + getName() + "  " +
                org.apache.commons.lang3.StringUtils.repeat('=', postLength) +
                " #";
    }

    @Override
    public SortedSet<Notification> getNotifications() {
        return notifications;
    }

    @Override
    public MessagesBundle getMessages() {
        return messages;
    }

    @Override
    public void setExpModifier(double expModifier) {
        this.expModifier = expModifier;
    }

    @Override
    public int getExpLimit() {
        return expLimit;
    }

    @Override
    public void setExpLimit(int expLimit) {
        this.expLimit = expLimit;
    }

    @Override
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public SkillManager getManager() {
        return manager;
    }

    @Override
    public final int getId() {
        return id;
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public String getDisplayName() {
        return displayName == null || displayName.isEmpty() ? name : displayName;
    }
    @Override
    public boolean hasDisplayName() {
        return displayName != null && !displayName.isEmpty();
    }

    @Override
    public RPGme getPlugin() {
        return manager.getPlugin();
    }

    @Override
    public String getDescription() {
        return messages.getMessageOr("description", "");
    }

    @Override
    public double getExpMultiplier() {
        return expModifier;
    }

    @Override
    public boolean isEnabled() {
        return config.getBoolean("enabled");
    }

    @Override
    public boolean isEnabled(Player p) {
        return isEnabled() && PermissionChecker.isEnabled(p, this);
    }

    @Override
    public boolean isEnabled(RPGPlayer rp) {
        return isEnabled() && PermissionChecker.isEnabled(rp, this);
    }

    @Override
    public float getExp(Player p) {
        return getPlugin().getPlayerManager().getExp(p, id);
    }

    @Override
    public float getExp(RPGPlayer p) {
        return p.getExp(id);
    }

    @Override
    public void addPersonalNotification(RPGPlayer player, Notification note) {
        player.addNotification(getId(), note);
    }

    @Override
    public void sendNotifications(RPGPlayer player, int level) {
        sendNotifications(player.getPlayer(), player.getPersonalNotifications(getId()), level);
        sendNotifications(player.getPlayer(), notifications, level);
    }

    private void sendNotifications(Player player, SortedSet<Notification> set, int level){
        for(Notification note : set) {
            if(note.getLevel() == level) {
                printNotification(player.getPlayer(), note);
            } else if(note.getLevel() != Notification.LVL_NONE && note.getLevel() > level) {
                break;
            }
        }
    }

    @Override
    public void addExp(RPGPlayer p, float exp) {
        p.addExp(getId(), exp);
    }

    @Override
    public void addExp(Player p, float exp) {
        RPGPlayer player = manager.getPlugin().getPlayer(p);
        if(player != null)
            player.addExp(getId(), exp);
    }

    @Override
    public RPGPlayer getPlayer(Player p) {
        return getPlugin().getPlayer(p);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <A extends Ability> A getAbility(int id) {
        try {
            return (A) abilityMap.get(id);
        } catch (ClassCastException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Iterable<Ability> getAbilities() {
        return abilityMap.values();
    }

    @Override
    public void registerAbility(int id, Ability ability) {
        Ability other = abilityMap.get(id);
        if(other != null) {
            getPlugin().getLogger().severe("Failed to register module "+ ability.getClass() + ". Id " + id + " already taken by " + other.getClass());
        } else {

            boolean isAbilityEnabled = getManager().getSkillConfig() == null || getManager().getSkillConfig().getBoolean(getName() + "." + ability.getName() + ".enabled", true);
            if(!isAbilityEnabled) {
                RPGme.debug(ability.getName() + " not enabled");
                return;
            }

            try {
                ability.onEnable();
            } catch(Exception e) {
                getPlugin().getLogger().log(Level.SEVERE, "Failed to enable ability " + ability.getClass(), e);
                return;
            }
            getPlugin().getServer().getPluginManager().registerEvents(ability, getPlugin());
            abilityMap.put(id, ability);
        }
    }

    @Override
    public Ability removeAbility(int id) {
        Ability ability = abilityMap.remove(id);
        if(ability == null)
            return null;

        HandlerList.unregisterAll(ability);

        try {
            ability.onDisable();
        } catch (Exception e) {
            getPlugin().getLogger().log(Level.SEVERE, "Exception while disabling ability " + ability.getClass(), e);
        }
        return ability;
    }

    @Override
    public int getLevel(Player p) {
        return manager.getPlugin().getLevel(p, id);
    }

    @Override
    public int getLevel(RPGPlayer p) {
        return p.getLevel(id);
    }

    @Override
    public ConfigurationSection getConfig() {
        return config;
    }

    public void addNotification(int level, String prefix, String title, String text) {
        notifications.add(new Notification(level, prefix, title, text));
    }

    public void addNotification(Notification.Builder builder) {
        addNotification(builder.build());
    }

    @Override
    public void addNotification(Notification note) {
        notifications.add(note);
    }

    @Override
    public void printNotification(Player p, Notification notification) {
        List<String> list = new ArrayList<String>();

        String color = ChatColor.DARK_GREEN.toString();
        list.add(color + Symbol.FRAME_TOP);
        list.add(color + Symbol.FRAME_LINE + notification.getTitle());
        list.addAll(StringUtils.splitToLength(color + Symbol.FRAME_LINE + manager.colorLight(), notification.getText(), " ", 50));
        list.add(color + Symbol.FRAME_END);

        String[] result = list.toArray(new String[list.size()]);
        p.sendMessage(result);
        new HarpPlingRunnable(p).start();
    }

    /**
     * Builds and sends the chat text shown in the /<skill> command.
     */
    @Override
    public void showCommandText(Player p) {
        if(!isEnabled(p)) {
            p.sendMessage("Unknown Skill.");
            return;
        }

        RPGPlayer rp = manager.getPlugin().getPlayer(p);
        int level = getLevel(rp);

        // The regular messages that is used by this skill is not the 'root' of the bundle
        // All keys getEntry prefixes so that they are unique to the skill.

        // For the ui_ keys though we need the root, which you can getEntry like so:
        MessagesBundle pluginMessages = getPlugin().getMessages();

        // top
        String top = "&7" + Symbol.FRAME_TOP +
                "\n&7" + Symbol.FRAME_LINE + "&2&l" + getDisplayName() + "  &r&7"+ Symbol.ARROW_RIGHT + " lvl " +  manager.colorLight() + level +
                "\n&3" + Symbol.FRAME_LINE;
        ComponentBuilder builder = new ComponentBuilder(StringUtils.colorize(top));

        // exp bar
        int start = Math.max(0, ExpTables.xpForLevel(level));
        double current = getExp(rp);
        int end = ExpTables.xpForLevel(level+1);

        String percent = String.format("%.1f", (current - start) / (end - start) * 100.0);
        //int percent = (int) ( (current-start)/(end-start)*100) ;

        String exp = ((int) current == current) ? String.valueOf((int) current) : String.format("%.2f", current);
        String hoverText = "&7" + exp  + " / " + end + " (&f" + percent + "%&7)\n" +
                pluginMessages.getMessage("ui_nextlevel") + " &b" + (end-(int)current) + " exp";
        builder.append(buildExpBar(start, current, end)).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(StringUtils.colorize(hoverText)).create()));

        // description
        List<String> description = StringUtils.colorize(StringUtils.splitToLength("&7" + Symbol.FRAME_LINE + "&f", getDescription(), " ", 55));
        builder.append("\n", ComponentBuilder.FormatRetention.NONE);
        for(String s : description) {

            builder.append(s+"\n");
        }

        // url
        String url = this.messages.getMessage("url");
        if(url != null) {
            String urlLine = StringUtils.colorize("&e" + Symbol.FRAME_LINE + " &6" + Symbol.ARROW_RIGHT2 + " " + pluginMessages.getMessage("ui_url")) + "\n";
            builder.append(urlLine).event(new ClickEvent(ClickEvent.Action.OPEN_URL, url));
        }

        // current stats
        List<String> stats = StringUtils.colorize(getCurrentStats(level));

        if(!stats.isEmpty()) {
            builder.append(StringUtils.colorize("&6"+Symbol.FRAME_LINE + pluginMessages.getMessage("ui_stats")), ComponentBuilder.FormatRetention.NONE);

            for(String s : stats) {
                String[] part = s.split(":");
                if(part.length > 1) {

                    String key = part[0];
                    String text = StringUtils.breakAndInsert(part[1], "&6" + Symbol.FRAME_LINE + org.apache.commons.lang3.StringUtils.repeat(' ', key.length() *16/10) + "&b", false, " ", 60);
                    builder.append(StringUtils.colorize("\n&6" + Symbol.FRAME_LINE + " &2"+ Symbol.ARROW_RIGHT2 + " &9"+key + "&7 : &b" + text));
                }
            }
            builder.append("\n");
        }

        // previous notifications
        builder.append(ChatColor.GREEN + Symbol.FRAME_LINE + pluginMessages.getMessage("ui_messages") + "\n");


        Set<Notification> set;
        if(!rp.getPersonalNotifications(getId()).isEmpty()) {
            set = new TreeSet<>(notifications);
            set.addAll(rp.getPersonalNotifications(getId()));
        } else {
            set = notifications;
        }

        for(Notification notification : set) {

            int forLevel = notification.getLevel();
            if(forLevel == Notification.LVL_NONE || forLevel <= level) {

                if(!notification.isSticky())
                    continue;

                String title = pluginMessages.getMessage("ui_notification_title_format", notification.getIcon(), notification.getTitle(), forLevel == Notification.LVL_NONE ? "" : " lvl " + forLevel);
                builder.append(ChatColor.GREEN + Symbol.FRAME_LINE + title);

                ComponentBuilder hover = new ComponentBuilder("");
                List<String> text = StringUtils.splitToLength("&7"+ Symbol.FRAME_LINE + manager.colorDark(), notification.getText(), " ", 50);
                for(String s : StringUtils.colorize(text)) {
                    hover.append(s + "\n");
                }

                builder.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, hover.create()));
                builder.append("\n", ComponentBuilder.FormatRetention.NONE);
            } else {

                String title = pluginMessages.getMessage("ui_next", notification.getTitle(), forLevel);
                builder.append(Symbol.FRAME_LINE).color(net.md_5.bungee.api.ChatColor.GRAY).append("   ").append(StringUtils.colorize(title) + "\n");
                break;
            }

        }

        builder.append(Symbol.FRAME_END + "\n").color(net.md_5.bungee.api.ChatColor.GRAY);

        p.spigot().sendMessage(builder.create());

    }

    private List<String> getCurrentStats(int forlevel) {
        List<String> list = new ArrayList<>(abilityMap.size() + 1);

        addCurrentStatistics(forlevel, list);
        for(StatProvider stat : abilityMap.values()) {
            stat.addCurrentStatistics(forlevel, list);
        }
        return list;
    }

    public static String buildExpBar(int startxp, double curxp, int targetxp) {
        int count = 35;

        int blue = (int) Math.round(Math.max(0, (count * (curxp - startxp) / (targetxp - startxp)  )));

        return StringUtils.colorize("&3[&b&m" + org.apache.commons.lang3.StringUtils.repeat('=', blue) + "&7&m" + org.apache.commons.lang3.StringUtils.repeat('-', count-blue) + "&r&3]");
    }

}
