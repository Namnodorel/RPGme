package com.rpgme.plugin.util.effect;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.util.GameSound;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class HarpPlingRunnable extends BukkitRunnable {

	private Location location;
	private Player player;
	
	private int step = 0;
		
	public HarpPlingRunnable(Location location) {
		this.location = location;
	}

	public HarpPlingRunnable(Player player) {
		this.player = player;
	}
	
	public void start() {
		runTaskTimer(RPGme.getInstance(), 0l, 3l);
	}

	@Override
	public void run() {
		if(step++ == 5) {
			cancel();
			return;
		}
		
		if(step == 2)
			return;
		
		float pitch = 0.8f + step * 0.15f;
		
		if(player != null)
			GameSound.play(Sound.BLOCK_NOTE_HARP, player, 1.4f, pitch);
		else if(location != null)
			GameSound.play(Sound.BLOCK_NOTE_HARP, location, 1.8f, pitch);

	}

}
