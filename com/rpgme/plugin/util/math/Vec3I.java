package com.rpgme.plugin.util.math;

import org.bukkit.Location;

public class Vec3I implements Cloneable {
	
	public final int x,y,z;
	
	public Vec3I(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vec3I(Vec3I vec) {
		this(vec.x, vec.y, vec.z);
	}
	
	public Vec3I(Location loc) {
		this(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
	}
	
	public Vec3I add(Vec3I vec) {
		return new Vec3I(x + vec.x, y + vec.y, z + vec.z);
	}
	
	public Vec3I add(int x, int y, int z) {
		return new Vec3I(this.x + x, this.y + y, this.z + z);
	}
	
	public Vec3I substract(Vec3I vec) {
		return new Vec3I(x - vec.x, y - vec.y, z - vec.z);
	}
	public Vec3I substract(int x, int y, int z) {
		return new Vec3I(this.x - x, this.y - y, this.z - z);
	}
	
	public Vec3I scale(double scale) {
		return new Vec3I((int) Math.round(x * scale), (int) Math.round(y * scale), (int) Math.round(z * scale));
	}
		
	public double distanceSquared(Vec3I other) {
		return Math.abs( substract(other).lengthSquared() );
	}
	
	public double distance(Vec3I other) {
		return Math.sqrt(distanceSquared(other));
	}
	
	public int lengthSquared()
	{
		return x * x + y * y + z * z;
	}

	public double length()
	{
		return Math.sqrt(lengthSquared());
	}
	
	public Vec3I normalise()
	{
		return scale(1.0f / length());
	}
	
	public static int dot(Vec3I a, Vec3I b) {
		return a.x * b.x + a.y * b.y + a.z * b.z;
	}
	
	@Override
	public String toString()
	{
		return "Vector[x=" + x + ", y=" + y + ", z=" + z + "]";
	}
	
	public boolean equals(Vec3I other) {
		return x == other.x && y == other.y && z == other.z;
	}
	
	@Override
	public Vec3I clone() {
		return new Vec3I(this);
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		return equals((Vec3I)obj); 
	}
	
	@Override
	public int hashCode() {
		return (x*y*z);
	}

}
