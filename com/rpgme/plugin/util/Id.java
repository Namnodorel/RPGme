package com.rpgme.plugin.util;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 */
public final class Id {

    private static final AtomicInteger id = new AtomicInteger(1);

    public static int newId() {
        return id.addAndGet(1);
    }

}
