package com.rpgme.plugin.util.cooldown;

import com.google.common.collect.Maps;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

public class VarMaxEnergyCooldown implements Cooldown {
	
	private final Map<UUID, Entry> map = Maps.newHashMap();
	private final int energypersec, energycost;

	public VarMaxEnergyCooldown(Plugin plugin, int enerypersec, int energycost) {
		CooldownCleaner.register(this, plugin);
		this.energypersec = enerypersec;
		this.energycost = energycost;
	}

	@Override
	public boolean isOnCooldown(Player p) {
		Entry e = map.get(p.getUniqueId());
		if(e == null)
			return false;
		return e.getCurrentEnergy() < energycost;
	}

	@Override
	public void add(Player p) {
		throw new UnsupportedOperationException("Use register(Player, int) instead.");
	}
	
	@Override
	public void add(Player p, long maxenergy) {
		Entry entry = map.get(p.getUniqueId());
		if(entry == null) {
			entry = new Entry((int) maxenergy);
			map.put(p.getUniqueId(), entry);
		} else {
			entry.maxEnergy = (int) maxenergy;
		}
		entry.onUse();
	}
	
	@Override
	public boolean remove(Player p) {
		return map.remove(p.getUniqueId()) != null;
	}

	@Override
	public long getMillisRemaining(Player p) {
		Entry e = map.get(p.getUniqueId());
		if(e == null)
			return 0l;
		double current = e.getCurrentEnergy();
		if(current >= energycost)
			return 0l;
		
		return (long) ((energycost - current) / energypersec * 1000l); 
	}

	@Override
	public void cleanUp() {
		
		synchronized(map) {
			Iterator<Map.Entry<UUID, Entry>> it = map.entrySet().iterator();
			while(it.hasNext()) {
				Entry e = it.next().getValue();
				if(e.energy == e.maxEnergy)
					it.remove();
				
			}
		}
		
	}
	
	public class Entry {
		
		private long lastUsed;
		private double energy;
		private int maxEnergy;
		
		public Entry(int maxEnergy) {
			this.lastUsed = System.currentTimeMillis();
			this.maxEnergy = maxEnergy;
			this.energy = maxEnergy;
		}

		double getCurrentEnergy() {
			return Math.min(maxEnergy, (energy + ((System.currentTimeMillis() - lastUsed) / 1000.0 * energypersec)) );
		}
		
		void onUse() {
			energy = getCurrentEnergy() - energycost;
			lastUsed = System.currentTimeMillis();
		}

	}

}
