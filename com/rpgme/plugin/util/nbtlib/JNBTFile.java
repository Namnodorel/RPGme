package com.rpgme.plugin.util.nbtlib;

import org.bukkit.Bukkit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class JNBTFile {

	// the file to use
	private final File file;
	// the TagCompound containing the files content
	private Tag tag;

	public JNBTFile(File file) {
		this.file = file;
	}
	// two optional constructors:
	public JNBTFile(String folder, String name) {
		this(new File(folder, name));
	}

	public JNBTFile(String path) {
		this(new File(path));
	}

	public void clear() {
		tag = new CompoundTag();
	}
	
	public boolean exists() {
		return file.exists();
	}
	
//	public Tag getTag() {
//		return compound;
//	}
	
	@SuppressWarnings("unchecked")
	public <T extends Tag> T getTag() {
		return (T) tag;
	}
	
	public void setTag(Tag tag) {
		this.tag = tag;
	}
	
	public void read() {
		try {
			// if the file exists we read it
			if(file.exists()) {
				NBTInputStream fileinputstream = new NBTInputStream(new FileInputStream(file));
				tag = fileinputstream.readTag();
				fileinputstream.close();
				
			} else {
			// else we create an empty TagCompound
				clear();
			}
		} catch(IOException e) {
			Bukkit.getLogger().severe("Error while reading file '"+file+ '\'');
			e.printStackTrace();
		}
	}

	public void write() {
		try {

			if(!file.exists()) {
				file.createNewFile();
			}

			NBTOutputStream fileoutputstream = new NBTOutputStream(new FileOutputStream(file));
			fileoutputstream.writeTag(tag);
			fileoutputstream.close();

		} catch(IOException e) {
			Bukkit.getLogger().severe("Error while saving file '"+file+ '\'');
			e.printStackTrace();
		}
	}
	
	public void delete() {
		file.delete();
	}

}
