package com.rpgme.plugin.api;

import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import org.bukkit.configuration.ConfigurationSection;

/**
 <p>A Module is a small part of the RPGme plugin. When registered. the plugin will send events and lifecycle callbacks.
 Modules should be registered during the server enable period.</p>
 <p>A Module can be a Skill, an ability of a skill, or a global, background, feature.</p>
 */
public interface Module {

    default String getName() {
        return getClass().getSimpleName();
    }

    default String getConfigDescription() {
        return "";
    }

    /**
     * This method is called when the module is enabling. This is the only callback in which it is allowed to register additional (sub)modules.
     * Note that during this callback, you do not yet have access to the various configurations.
     * If your class also implements {@link org.bukkit.event.Listener}, then it will be automatically registered by the plugin.
     * See {@link com.rpgme.plugin.api.ListenerModule}
     */
    default void onEnable() { }


    /**
     * This method is called when the plugin should reload. In this method all modules define all the configurable values.
     * You add values to the config and bundle builder parameters using the <i>add</i> methods. These will then be synced with configuration files on disk for server owners to change.
     * You can use any strings as keys, they do not have to be unique for this module. The correct modules paths will automagically be added. For example the key "enabled" with still be specific for this module.
     * Read the values you added here in the {@link #onLoad(ConfigurationSection, BundleSection)} callback.
     * @param config write values in here to sync with config.yml or skills.yml files.
     * @param messages write values in here to sync with messages.properties. This should be used for all Strings to be displayed to users, so they can be translated.
     */
    default void createConfig(ConfigBuilder config, BundleBuilder messages) { }

    /**
     * Called when the configuration is done reading. Use the parameter objects to read the values you defined before.
     * <b>Only use keys that you also used in {@link #createConfig(ConfigBuilder, BundleBuilder)}</b>
     * The paramater objects are also safe to store if you'd want to. This is not needed for ubclasses of {@link com.rpgme.plugin.skill.BaseSkill} or {@link com.rpgme.plugin.skill.Ability}.
     * They can use the protected <i>config</i> and <i>messages</i> fields in those classes.
     * @param config the configuration section to read from. This is the same section as used by the builder before.
     * @param messages the messages section to read from. This is the same section as used by the builder before.
     */
    default void onLoad(ConfigurationSection config, BundleSection messages) { }

    /**
     * Called when the module should disable.
     * Use this to clean up or save persistent data.
     */
    default void onDisable() { }

}
