package com.rpgme.plugin.event;

import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.player.RPGPlayer;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class RPGLevelupEvent extends PlayerEvent {
	
	private static final HandlerList handlers = new HandlerList();

    private final RPGPlayer rpgPlayer;
	private final Skill skill;
	private final int toLevel;

	public RPGLevelupEvent(RPGPlayer player, Skill skill, int toLevel) {
		super(player.getPlayer());
        this.rpgPlayer = player;
		this.skill = skill;
		this.toLevel = toLevel;
	}

    public RPGPlayer getRpgPlayer() {
        return rpgPlayer;
    }

    public Skill getSkill() {
		return skill;
	}

	public int getSkillId() {
		return skill.getId();
	}

	public int getToLevel() {
		return toLevel;
	}
	
	public int getFromLevel() {
		return toLevel -1;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}


}
