package com.rpgme.plugin;

import com.rpgme.content.module.ChatFormatter;
import com.rpgme.content.module.Healthbars;
import com.rpgme.content.module.fireworks.FireworkShows;
import com.rpgme.content.module.highscore.HighscoreModule;
import com.rpgme.content.module.levelup.EffectsManager;
import com.rpgme.content.module.moblevel.MonsterLevels;
import com.rpgme.content.module.trails.SkillTrails;
import com.rpgme.content.skill.alchemy.Alchemy;
import com.rpgme.content.skill.archery.Archery;
import com.rpgme.content.skill.attack.Attack;
import com.rpgme.content.skill.defence.Defence;
import com.rpgme.content.skill.enchanting.Enchanting;
import com.rpgme.content.skill.farming.Farming;
import com.rpgme.content.skill.fishing.Fishing;
import com.rpgme.content.skill.forging.Forging;
import com.rpgme.content.skill.landscaping.Landscaping;
import com.rpgme.content.skill.mining.Mining;
import com.rpgme.content.skill.stamina.Stamina;
import com.rpgme.content.skill.taming.Taming;
import com.rpgme.content.skill.woodcutting.Woodcutting;
import com.rpgme.plugin.api.Module;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.blockmeta.BlockDataManager;
import com.rpgme.plugin.command.ItemCommands;
import com.rpgme.plugin.command.RPGmeCommand;
import com.rpgme.plugin.command.SkillDisplayCommand;
import com.rpgme.plugin.command.SkillOverviewCommand;
import com.rpgme.plugin.event.GeneralEventListener;
import com.rpgme.plugin.manager.CommandManager;
import com.rpgme.plugin.manager.ModuleManager;
import com.rpgme.plugin.manager.SkillManager;
import com.rpgme.plugin.player.PartyModule;
import com.rpgme.plugin.player.PlayerManager;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.treasure.TreasureBag;
import com.rpgme.plugin.util.Id;
import com.rpgme.plugin.util.RPGRunnable;
import com.rpgme.plugin.util.config.*;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

import static com.rpgme.content.skill.SkillType.*;

/**
 * Main class. This (indirectly) handles all modules of the plugin. Additionally it handles the configuration logic and has some quick access api methods.
 */
public class RPGme extends JavaPlugin {

    // core modules
    public static final int MODULE_PLAYER_MANAGER = Id.newId();
    public static final int MODULE_COMMAND_MANAGER = Id.newId();
    public static final int MODULE_BLOCK_META = Id.newId();
    public static final int MODULE_TREASURES = Id.newId();

    // additional default modules
    public static final int MODULE_PARTIES = Id.newId();
    public static final int MODULE_HIGHSCORES = Id.newId();
    public static final int MODULE_HEALTH_BARS = Id.newId();
    public static final int MODULE_CHAT_PLACEHOLDERS = Id.newId();
    public static final int MODULE_MOB_LEVELS = Id.newId();
    public static final int MODULE_LEVELUP_EFFECTS = Id.newId();
    public static final int MODULE_SKILL_TRAILS = Id.newId();
    public static final int MODULE_FIREWORK_SHOWS = Id.newId();

    /* default Skill ids are defined in SkillType.java */

    /* default Ability ids are defined in their parents Skill class */

    // singleton
    private static RPGme instance;
    public static RPGme getInstance() {
        return instance;
    }

    // configuration
    private FileConfiguration fileConfig;
    private MessagesBundle messagesBundle;

    // module storage
    private ModuleManager<Module> moduleManager;
    private SkillManager skillManager;

    @Override
    public void onEnable() {
        instance = this;
        ConfigHelper.prepare(this);

        moduleManager = new ModuleManager<>(this);
        skillManager = new SkillManager(this);

        // register core modules
        registerModule(MODULE_PLAYER_MANAGER, new PlayerManager(this));
        registerModule(MODULE_COMMAND_MANAGER, new CommandManager(this));
        registerModule(MODULE_BLOCK_META, new BlockDataManager(this));
        registerModule(MODULE_TREASURES, new TreasureBag(this));

        // skills
        skillManager.registerModule(ALCHEMY, new Alchemy());
        skillManager.registerModule(ARCHERY, new Archery());
        skillManager.registerModule(ATTACK, new Attack());
        skillManager.registerModule(DEFENCE, new Defence());
        skillManager.registerModule(ENCHANTING, new Enchanting());
        skillManager.registerModule(FARMING, new Farming());
        skillManager.registerModule(FISHING, new Fishing());
        skillManager.registerModule(FORGING, new Forging());
        skillManager.registerModule(LANDSCAPING, new Landscaping());
        skillManager.registerModule(MINING, new Mining());
        skillManager.registerModule(STAMINA, new Stamina());
        skillManager.registerModule(TAMING, new Taming());
        skillManager.registerModule(WOODCUTTING, new Woodcutting());

        // extra modules
        registerModule(MODULE_PARTIES, new PartyModule(this));
        registerModule(MODULE_HIGHSCORES, new HighscoreModule(this));
        registerModule(MODULE_HEALTH_BARS, new Healthbars(this));
        registerModule(MODULE_MOB_LEVELS, new MonsterLevels(this));
        registerModule(MODULE_CHAT_PLACEHOLDERS, new ChatFormatter());
        registerModule(MODULE_LEVELUP_EFFECTS, new EffectsManager());
        registerModule(MODULE_SKILL_TRAILS, new SkillTrails());
        registerModule(MODULE_FIREWORK_SHOWS, new FireworkShows(this));

        // commands
        CommandManager commandManager = getCommandManager();
        commandManager.register(new RPGmeCommand(this));
        commandManager.register(new SkillDisplayCommand(this));
        commandManager.register(new SkillOverviewCommand(this));
        commandManager.register(new ItemCommands(this));

        new GeneralEventListener(this).registerListeners();

        // set these modules so they do not get an generated option to be disabled
        moduleManager.setCannotBeDisabled(MODULE_PLAYER_MANAGER, MODULE_COMMAND_MANAGER, MODULE_BLOCK_META, MODULE_TREASURES);

        // finally read and apply the configuration
        /* note: every plugin that adds modules has to call this reloadConfig in the end to commit their modules */
        reloadConfig();
    }

    @Override
    public void onDisable() {
        moduleManager.onDisable();
        skillManager.onDisable();

        RPGRunnable.finishAll();
    }

    /**
     * Get one of the registered modules. All of the default module ids are declared in this class.
     * To overwrite the behaviour of a module, you can remove the existing module first, then register your own version with the same id.
     * If you do this, be sure that your new class extends the original, so that methods depending on it will not break.
     * @param id of the module
     * @param <T> the module instance
     * @return The module instances casted to the type you want. If a ClassCastException occurs this method returns null instead.
     */
    @SuppressWarnings("unchecked")
    public <T extends Module> T getModule(int id) {
        return (T) moduleManager.getById(id);
    }

    /**
     * <p>Register a module to the plugin. This should be used for global features, not bound to a specific Skill.
     * Skills themselves (which are also Modules) can be added here, though they are just forwarded to the SkillManager ({@link SkillManager#registerModule(int, Module)})
     * Abilities should be registered in the parent BaseSkill class.</p>
     * <p>Note that this will fail if there is already a module registered for a given id. If you want to replace a module, you have to disable it first with {@link #removeModule(int)}</p>
     * @param id The id to register this module to.
     * @param module the module instance to register.
     * @return false if id already taken or an exception occurred during enable, true otherwise
     */
    public boolean registerModule(int id, Module module) {
        if(module instanceof Skill) {
            return getSkillManager().registerModule(id, (Skill) module);
        }

        if(module instanceof Ability) {
            Ability ability = (Ability) module;
            ability.getSkill().registerAbility(id, ability);
            return true;
        }

        return moduleManager.registerModule(id, module);
    }

    /**
     * Removes and unregisters a Module for the given id.
     * This may be a root module, skill or ability.
     * @param id id the module was registered with
     * @return the removed module, or null if none found.
     */
    public Module removeModule(int id) {

        if(moduleManager.containsId(id)) {
            return moduleManager.removeModule(id);
        }

        // if not a root module, it may be a skill
        Skill skill = getSkillManager().removeModule(id);
        if(skill != null) {
            return skill;
        }

        // if not a skill, it may be a ability
        for(Skill s : getSkillManager().getEnabledSkills()) {

            Ability ability = s.removeAbility(id);
            if(ability != null) {
                return ability;
            }
        }
        return null;
    }

    /** api / shortcut methods */

    public ModuleManager<Module> getModuleManager() {
        return moduleManager;
    }

    public SkillManager getSkillManager() {
        return skillManager;
    }

    public PlayerManager getPlayerManager() {
        return getModule(MODULE_PLAYER_MANAGER);
    }

    public CommandManager getCommandManager() {
        return getModule(MODULE_COMMAND_MANAGER);
    }

    public RPGPlayer getPlayer(Player player) {
        return getPlayerManager().get(player);
    }

    public int getLevel(Player player, int skill) {
        return getPlayer(player).getLevel(skill);
    }

    public float getExp(Player player, int skill) {
        return getPlayer(player).getExp(skill);
    }

    public Skill getSkill(String name) {
        return getSkillManager().getByName(name);
    }

    public Skill getSkill(int id) {
        return getSkillManager().getById(id);
    }

    public void addExp(Player player, int skill, float exp) {
        Skill instance = getSkillManager().getById(skill);
        if(instance != null) {
            instance.addExp(player, exp);
        }
    }

    /* configuration logic */

    @Override
    public FileConfiguration getConfig() {
        return fileConfig;
    }

    public MessagesBundle getMessages() {
        return messagesBundle;
    }


    /** Our custom reload method. It build the configurations according to registered modules, and then syncs it with the config.yml and messages.properties files. */
    @Override
    public void reloadConfig() {
        // here all modules define the configuration
        ConfigBuilder configBuilder = new ConfigBuilder();
        ConfigBuilder skillBuilder = new ConfigBuilder();
        BundleBuilder bundleBuilder = new BundleBuilder();

        // define all the entire configuration files
        configBuilder.addLargeHeader("RPGme Plugin configuration");
        moduleManager.createConfig(configBuilder, bundleBuilder);
        skillManager.createConfig(skillBuilder, bundleBuilder);

        // This creates the file if not exist, reads the values, and updates the file if there are new values in our defined target config.
        // The settings configured in the file are now in the configBuilder
        File file = new File(getDataFolder(), "config.yml");
        configBuilder.combineWith(file);
        // use it as our config
        fileConfig = configBuilder.build();

        file = new File(getDataFolder(), "skills.yml");
        skillBuilder.combineWith(file);

        // now for the messages
        file = new File(getDataFolder(), "messages.properties");
        bundleBuilder.combineWith(file);
        messagesBundle = bundleBuilder.build();
        BundleSection messages = messagesBundle.getSection("");

        // now we have both configurations complete. Time to let the modules know so they can update their values in onLoad()
        moduleManager.onLoad(fileConfig, messages);
        skillManager.onLoad(skillBuilder.build(), messages);
    }


    public File getUserDataFolder() {
        return new File(getDataFolder(), "data");
    }

    public static boolean DEBUG = true;
    public static void debug(String message) {
        if(DEBUG) {
            getInstance().getServer().getConsoleSender().sendMessage(ChatColor.AQUA + "Debug; " + ChatColor.WHITE + message);
        }
    }
    public static void debug(String message, Object... args) {
        if(DEBUG) {
            getInstance().getServer().getConsoleSender().sendMessage(ChatColor.AQUA + "Debug; " + ChatColor.WHITE + String.format(message, args));
        }
    }
}
